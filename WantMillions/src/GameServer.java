import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GameServer {

    private final static int DEFAULT_PORT = 80;
    private static final ExecutorService threadManager = Executors.newCachedThreadPool();
    private final List<ServerPlayer> players = Collections.synchronizedList(new ArrayList<>());
    private int currentPlayers = 0;
    private int maxNames = 0;
    private int maxPlayers;


    public static void main(String[] args) {

        int port = DEFAULT_PORT;

        try {

            if (args.length > 0) {
                port = Integer.parseInt(args[0]);
            }

            GameServer gameServer = new GameServer();
            gameServer.start(port, 2);

        } catch (NumberFormatException ex) {
            System.exit(1);
        }
    }


    public void start(int port, int maxPlayers) {

        this.maxPlayers = maxPlayers;

        try {
            ServerSocket serverSocket = new ServerSocket(port);

            while (currentPlayers != maxPlayers) {

                System.out.println("Server ready and waiting for Players...");

                Socket clientSocket = serverSocket.accept();

                System.out.println("Player connected...");

                ServerPlayer player = new ServerPlayer(clientSocket, new Questions());

                currentPlayers++;
                players.add(player);
            }

            System.out.println("All Players ready...");

            for (ServerPlayer players : players) {

                threadManager.submit(players);
            }
        } catch (IOException e) {
            System.out.println("Unable to start Game");
        }
    }


    //TODO--------------------------------------------------------------------------------------------------------------

    private class ServerPlayer implements Runnable {


        private String name = null;
        final private Socket clientSocket;
        final private BufferedReader in;
        final private BufferedWriter out;
        private int score;
        private Questions question;
        Prompt prompt = null;

        public ServerPlayer(Socket clientSocket, Questions question) throws IOException {

            this.question = question;

            prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            question.setPrompt(this.prompt);
            question.setIn(in);
            question.setOut(out);

            this.clientSocket = clientSocket;
        }

        private String listOfPlayers() {
            StringBuilder builder = new StringBuilder();

            for (ServerPlayer sw : players) {
                builder.append(sw.getName()).append("\n");
            }

            return builder.toString();
        }

        private void setPlayerScore() {
            for (ServerPlayer sw : players) {
                setScore(sw.question.getScore());
            }
        }

        synchronized private String getEndGameScores() {

            setPlayerScore();

            notifyAll();

            StringBuilder builder = new StringBuilder();
            for (ServerPlayer sw : players) {
                builder.append(sw.getName()).append("\n");
                builder.append(sw.getScore()).append("\n");
            }
            return builder.toString();
        }

        private void askName() {

            StringInputScanner question1 = new StringInputScanner();

            question1.setMessage("\n" + "OPEN IN FULLSCREEN PLEASE!" + "\n" + "\n" + "Player, what is your name?" + "\n");

            String name = prompt.getUserInput(question1);

            maxNames++;

            setName(name);


            try {
                out.write("\n" + "\n" + Menus.WELCOME + "\n" + "\n");
                out.flush();

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Error in askName method.");
            }
        }

        public void setScore(int score) {
            this.score = score;
        }

        private void rules() {

            try {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                out.write("\n" + "\n" + Menus.RULES + "\n" + "\n");
                out.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        public String getName() {
            return name;
        }


        public int getScore() {
            return score;
        }


        public void setName(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            askName();
            while (maxNames != maxPlayers) {
                try {
                    out.write(Menus.WAITING);
                    out.flush();
                    Utilitarian.waitTime(3000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                rules();
                Utilitarian.waitTime(10000);
                out.write(Menus.PLAYERS);
                out.write(listOfPlayers());
                out.flush();
                Utilitarian.waitTime(10000);
                Utilitarian.waitTime(1000);
                out.write(Menus.FIVE);
                out.flush();
                Utilitarian.waitTime(1000);
                out.write(Menus.FOUR);
                out.flush();
                Utilitarian.waitTime(1000);
                out.write(Menus.TREE);
                out.flush();
                Utilitarian.waitTime(1000);
                out.write(Menus.TWO);
                out.flush();
                Utilitarian.waitTime(1000);
                out.write(Menus.ONE);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            question.askQuestionGeo();
            try {
                out.write(getEndGameScores());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

