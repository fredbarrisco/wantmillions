import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.io.BufferedReader;
import java.io.BufferedWriter;


public class Questions {

    private Prompt prompt;
    private BufferedReader in;
    private BufferedWriter out;
    private int score = 0;

    String hisQuestion1 = "Qual era o rei cujo cognome era 'Boa Memória'?";
    String[] hisOption1 = {"D.João I", "D.Joao II", "D.Afonso V"}; //cognome "boa memoria"
    String hisQuestion2 = "Quantas pessoas de nacionalidade russa já pisaram solo lunar?";
    String[] hisOption2 = {"0", "1", "2"}; //pisar solo lunar russo
    String hisQuestion3 = "Que base americana foi atacada a 7 of December, 1941?";
    String[] hisOption3 = {"Pearl Harbor", "Hiroshima", "Batalha de Alam Halfa"};//pearl
    String hisQuestion4 = "Em que ano se tornou obrigatório o ensino primário?";
    String[] hisOption4 = {"1952", "1963", "1976"};// 1952
    String hisQuestion5 = "Quem nomeou Hitler Chanceler?";
    String[] hisOption5 = {"Friedrich Ebert", "Erich von Falkenhayn", "Paul von Hinderburg"};// 3 paul
    String hisQuestion6 = "Quem foi a primeira mulher a presidir o Parlamento Europeu?";
    String[] hisOption6 = {"Simone Veil", "Ursula von der Leyen", "Nicole Fontaine"};//, simone
    String hisQuestion7 = "Que rei tinha como cognome 'O Lavrador'?";
    String[] hisOption7 = {"D. Afonso VI", "D.Dinis", "D.Fernando"};//  Dinis
    String hisQuestion8 = "Quais deste acontecimentos se deu no dia 1 de Dezembro de 1640";
    String[] hisOption8 = {"Restauração da Independência", "Aclamação do Rei D.João V", "Implantação da República"}; //restauraçao da independencia
    String hisQuestion9 = "Em que ano foi a primeira edição do Mundial de futebol?";
    String[] hisOption9 = {"1920", "1930", "1940"}; //1930
    String hisQuestion10 = "Em que período da pré-história o fogo foi descoberto?";
    String[] hisOption10 = {"Neolítico", "Paleolítico", "Mesolítico"}; //Paleolítico


    String entreQuestion1 = "Em que ano foi o primerio WoodStock ?";
    String[] entreOption1 = {"1965", "1969", "1971"}; //Ano do primeiro Woodstock ,  69
    String entreQuestion2 = "Quanto óscares obteve o filme África Minha? ";
    String[] entreOption2 = {"6", "7", "9"}; // , 7
    String entreQuestion3 = "Quem lançou Chico Fininho?";
    String[] entreOption3 = {"Rui Veloso", "Zeca Afonso", "Pedro Abrunhosa"}; // ,  Rui
    String entreQuestion4 = "Em Rocco e os seus irmãos, quantos irmãos tinha?";
    String[] entreOption4 = {"4", "5", "6"}; // , 4
    String entreQuestion5 = "Em que ano os ABBA ganharam a Eurovisão?";
    String[] entreOption5 = {"1970", "1974", "1977"}; // , 1974
    String entreQuestion6 = "Como se chamava o canal de Herman José?";
    String[] entreOption6 = {"O canal do Tal", "O tal canal", "Herman e os compinchas"}; // ,  o tal canal
    String entreQuestion7 = "Quem foi o primeiro não humano a ganhar um Óscar?";
    String[] entreOption7 = {"Bugs Bunny", "Mafalda", "Mickey"}; // , Mickey
    String entreQuestion8 = "Que cantor/a intreperta o hit 'Who's that girl?' ?";
    String[] entreOption8 = {"Ruben Dias", "Britney Spears", "Madonna"}; // madonna
    String entreQuestion9 = "Quem era James Bond em 'James Bond contra o Doutor No' ?";
    String[] entreOption9 = {"Sean Connery", "Roger Moore", "Timothy Dalton"}; // , sean
    String entreQuestion10 = "Onde se realizam as festas do Colete Encarnado?";
    String[] entreOption10 = {"Vila Franca de Xira", "Moita", "Palmela"}; // , vfx

    String geoQuestion1 = "Qual é a lingua oficial de Andorra?";
    String[] geoOptions1 = {"Portinhol", "Castelhano", "Catalão"};   //Catalão
    String geoQuestion2 = "Qual é o estado independente mais pequeno do mundo?";
    String[] geoOptions2 = {"Vaticano", "Monaco", "Ilhas Fiji"};   // , Vaticano
    String geoQuestion3 = "Em que pais vivem os Malgaxes?";
    String[] geoOptions3 = {"Mauritânia", "Madagáscar", "Malta"};   // , Madagáscar
    String geoQuestion4 = "Qual é unidade monetária da Islândia?";
    String[] geoOptions4 = {"Lira", "Coroa", "Dinar"};   // , Lira
    String geoQuestion5 = "Que oceano banha as ilhas salomão?";
    String[] geoOptions5 = {"Ártico", "Pacífico", "Atlântico"};   // , Pacífico
    String geoQuestion6 = "Em que mar podemos encontrar um aligátor?";
    String[] geoOptions6 = {"Mar Vermelho", "Mar Negro", "Mar das Caraíbas"};   //Mar das Caraíbas
    String geoQuestion7 = "Com que nome de declarou independente o Paquistão Oriental?";
    String[] geoOptions7 = {"Uzbequistão", "Bangladesh", "Nepal"};   // , Bangladesh
    String geoQuestion8 = "Qual é o templo mais imponente de Adra, em pleno coração da índia?";
    String[] geoOptions8 = {"Taj Mahal", "Templo de Akshardham", "Laxminarayan"};   // , Taj Mahal
    String geoQuestion9 = "Onde fica situada a praia da Oura?";
    String[] geoOptions9 = {"Albufeira", "Lagos", "Portimão"};   // , Albufeira
    String geoQuestion10 = "De que marca de carros se fabricam mais modelos em Stuttgart?";
    String[] geoOptions10 = {"Mercedes", "Audi", "BMW"};   // , Mercedes

    String[] options1 = {"Algarve", "Lisboa", "Santarem"};
    String[] options2 = {"boobs", "ass", "feet"};

    String scieQuestion1 = "Qual é a vitamina que fixa o cálcio e o fósforo dos ossos?";
    String[] scieOption1 = {"Vitamina E", "Vitamina C", "Vitamina D"}; // , Vitaminda D
    String scieQuestion2 = "Qual é o maior músculo do corpo humano?";
    String[] scieOption2 = {"Gemeo", "Bicep", "Coxa"}; // Coxa
    String scieQuestion3 = "Em que constelação se encontra a Estrela Polar?";
    String[] scieOption3 = {"Cassiopeia", "Ursa Menor", "Ursa Maior"}; // Ursa Menor
    String scieQuestion4 = "Qual uma das principais causas de morte das cegonhas?";
    String[] scieOption4 = {"Fios de eletrecidade", "Caçadores", "Causas Naturais"}; // Fios de eletrecidade
    String scieQuestion5 = "De que cor é o pó do diamante?";
    String[] scieOption5 = {"Preto", "Amarelo", "Branco"}; // , Preto
    String scieQuestion6 = "Como se chama o laboratório espacial europeu?";
    String[] scieOption6 = {"Death Star", "SpaceStation", "SpaceLab"}; // , SpaceLab
    String scieQuestion7 = "Quantos ângulos iguais tem um triângulo escaleno?";
    String[] scieOption7 = {"0", "1", "3"}; // , 0
    String scieQuestion8 = "Que anéis descobriu Galileu?";
    String[] scieOption8 = {"Anéis de Jupiter", "Anéis de Saturno", "Anéis de Urano"}; // , Anéis de Saturno
    String scieQuestion9 = "Qual é a maior artéria?";
    String[] scieOption9 = {"Veia Cava Superior", "Braquial", "Aorta"}; // Aorta
    String scieQuestion10 = "O que são cumulo-nimbus?";
    String[] scieOption10 = {"Nuvens", "Marés", "Rochedos Montanhosos"}; // Nuvens


    synchronized public void askQuestionScience() {
        int result = 0;

        MenuInputScanner question1 = new MenuInputScanner(scieOption1);
        question1.setMessage(scieQuestion1);

        result = prompt.getUserInput(question1);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question2 = new MenuInputScanner(scieOption2);
        question2.setMessage(scieQuestion2);

        result = prompt.getUserInput(question2);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question3 = new MenuInputScanner(scieOption3);
        question3.setMessage(scieQuestion3);

        result = prompt.getUserInput(question3);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question4 = new MenuInputScanner(scieOption4);
        question4.setMessage(scieQuestion4);

        result = prompt.getUserInput(question4);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question5 = new MenuInputScanner(scieOption5);
        question5.setMessage(scieQuestion5);

        result = prompt.getUserInput(question5);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question6 = new MenuInputScanner(scieOption6);
        question6.setMessage(scieQuestion6);

        result = prompt.getUserInput(question6);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question7 = new MenuInputScanner(scieOption7);
        question7.setMessage(scieQuestion7);

        result = prompt.getUserInput(question7);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question8 = new MenuInputScanner(scieOption8);
        question8.setMessage(scieQuestion8);

        result = prompt.getUserInput(question8);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question9 = new MenuInputScanner(scieOption9);
        question9.setMessage(scieQuestion9);

        result = prompt.getUserInput(question9);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question10 = new MenuInputScanner(scieOption10);
        question10.setMessage(scieQuestion10);

        result = prompt.getUserInput(question10);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    synchronized public void askQuestionHistory() {

        MenuInputScanner question1 = new MenuInputScanner(hisOption1);
        question1.setMessage(hisQuestion1);

        int result = prompt.getUserInput(question1);
        System.out.println();

        if (result == 1) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question2 = new MenuInputScanner(hisOption2);
        question2.setMessage(hisQuestion2);

        result = prompt.getUserInput(question2);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question3 = new MenuInputScanner(hisOption3);
        question3.setMessage(hisQuestion3);

        result = prompt.getUserInput(question3);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question4 = new MenuInputScanner(hisOption4);
        question4.setMessage(hisQuestion4);

        result = prompt.getUserInput(question4);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question5 = new MenuInputScanner(hisOption5);
        question5.setMessage(hisQuestion5);

        result = prompt.getUserInput(question5);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question6 = new MenuInputScanner(hisOption6);
        question6.setMessage(hisQuestion6);

        result = prompt.getUserInput(question6);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question7 = new MenuInputScanner(hisOption7);
        question7.setMessage(hisQuestion7);

        result = prompt.getUserInput(question7);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question8 = new MenuInputScanner(hisOption8);
        question8.setMessage(hisQuestion8);

        result = prompt.getUserInput(question8);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question9 = new MenuInputScanner(hisOption9);
        question9.setMessage(hisQuestion9);

        result = prompt.getUserInput(question9);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }
        MenuInputScanner question10 = new MenuInputScanner(hisOption10);
        question10.setMessage(hisQuestion10);

        result = prompt.getUserInput(question10);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    synchronized public void askQuestionEntre() {

        MenuInputScanner question1 = new MenuInputScanner(entreOption1);
        question1.setMessage(entreQuestion1);

        int result = prompt.getUserInput(question1);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question2 = new MenuInputScanner(entreOption2);
        question2.setMessage(entreQuestion2);

        result = prompt.getUserInput(question2);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question3 = new MenuInputScanner(entreOption3);
        question3.setMessage(entreQuestion3);

        result = prompt.getUserInput(question3);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question4 = new MenuInputScanner(entreOption4);
        question4.setMessage(entreQuestion4);

        result = prompt.getUserInput(question4);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question5 = new MenuInputScanner(entreOption5);
        question5.setMessage(entreQuestion5);

        result = prompt.getUserInput(question5);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question6 = new MenuInputScanner(entreOption6);
        question6.setMessage(entreQuestion6);

        result = prompt.getUserInput(question6);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question7 = new MenuInputScanner(entreOption7);
        question7.setMessage(entreQuestion7);

        result = prompt.getUserInput(question7);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question8 = new MenuInputScanner(entreOption8);
        question8.setMessage(entreQuestion8);

        result = prompt.getUserInput(question8);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question9 = new MenuInputScanner(entreOption9);
        question9.setMessage(entreQuestion9);

        result = prompt.getUserInput(question9);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question10 = new MenuInputScanner(entreOption10);
        question10.setMessage(entreQuestion10);

        result = prompt.getUserInput(question10);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    synchronized public void askQuestionGeo() {

        MenuInputScanner question1 = new MenuInputScanner(geoOptions1);
        question1.setMessage(geoQuestion1);

        int result = prompt.getUserInput(question1);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question2 = new MenuInputScanner(geoOptions2);
        question2.setMessage(geoQuestion2);

        result = prompt.getUserInput(question2);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question3 = new MenuInputScanner(geoOptions3);
        question3.setMessage(geoQuestion3);

        result = prompt.getUserInput(question3);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question4 = new MenuInputScanner(geoOptions4);
        question4.setMessage(geoQuestion4);

        result = prompt.getUserInput(question4);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question5 = new MenuInputScanner(geoOptions5);
        question5.setMessage(geoQuestion5);

        result = prompt.getUserInput(question5);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question6 = new MenuInputScanner(geoOptions6);
        question6.setMessage(geoQuestion6);

        result = prompt.getUserInput(question6);
        if (result == 3) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question7 = new MenuInputScanner(geoOptions7);
        question7.setMessage(geoQuestion7);

        result = prompt.getUserInput(question7);
        if (result == 2) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question8 = new MenuInputScanner(geoOptions8);
        question8.setMessage(geoQuestion8);

        result = prompt.getUserInput(question8);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question9 = new MenuInputScanner(geoOptions9);
        question9.setMessage(geoQuestion9);

        result = prompt.getUserInput(question9);
        if (result == 1) {
            score += 10;
            System.out.println(score);
        }

        MenuInputScanner question10 = new MenuInputScanner(geoOptions10);
        question10.setMessage(geoQuestion10);

        result = prompt.getUserInput(question10);
        if (result == 1) {
            score += 10;
            System.out.println(score);
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void setPrompt(Prompt prompt) {
        this.prompt = prompt;
    }

    public void setOut(BufferedWriter out) {
        this.out = out;
    }

    public void setIn(BufferedReader in) {
        this.in = in;
    }

    public int getScore() {
        return score;
    }
}